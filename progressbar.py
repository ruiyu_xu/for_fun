#!usr/bin/env python

import sys
from time import sleep

for i in range(21):
    sys.stdout.write('\r')
    sys.stdout.write("[%-20s] %d%%" % ('='*i,5*i))
    sys.stdout.flush()
    sleep(0.2)
print '\r'
