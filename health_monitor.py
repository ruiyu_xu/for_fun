#!usr/bin/env python

import glob
import sys

class portfolio():
	"""
	This is the portfolio class
	Variables: name,sex,age,weight,height,cal_intake_list,cal_consume_list		
	Methods: cal_BMI, get_basic_calorie_info
	"""
	def __init__(self,*args):
		if len(args) == 1:
			file_name = args[0]
			self._initialize_port_with_file(file_name)
		else:
			self._initialize_port_with_info(args)
	
	def _initialize_port_with_file(self,file_name):
		"""
		Initialize the portfolio class with a portfolio file
		"""
		print "will use the file : ",file_name
		f_port = open(file_name,'r')
		lines = f_port.readlines()
		self.cal_intake_list = []
		self.cal_consume_list = []
		add_list = []
		for i, line in enumerate(lines):
			line = line.strip('\n')
			if line.startswith("Name"):
				self.name = line.split(":")[1]
			if line.startswith("Sex"):
				self.sex = line.split(":")[1]
			if line.startswith("Age"):
				self.age = line.split(":")[1]
			if line.startswith("Weight"):
				self.weight = float(line.split(":")[1])
			if line.startswith("Height"):
				self.height = float(line.split(":")[1])
			if "Calorie Intake" in line:
				pass
			if "Calorie Consumed" in line:
				self.cal_intake_list = add_list
				add_list = []
			if line.startswith("D"):
				add_list.append(float(line.split(":")[1]))
		self.cal_consume_list = add_list
		f_port.close()

	def _initialize_port_with_info(self,args):
		"""
		Initialize the portfolio class with a information entered
		"""
		self.name = args[0]
		self.sex = args[1]
		self.age = args[2]
		self.weight = float(args[3])
		self.height = float(args[4])
	 	self.cal_intake_list = [float(i) for i in args[5]]
		self.cal_consume_list = [float(i) for i in args[6]]

	def cal_BMI(self):
		"""
		This method calculate and return the BMI for this portfolio
		"""
		height_m = self.height/100
		return self.weight/(height_m*height_m)

	def get_basic_calorie_info(self):
		"""
		This method get basic moderate active calorie information
		for this portfolio
		"""
		f_basic = open("basic_info.txt",'r')
		entry_name = self._generate_entry_name()
		for line in f_basic.readlines():
			if not line.startswith(entry_name):
				continue
			else:
				line = line.strip('\n')
				return float(line.split("/")[2])

	def _generate_entry_name(self):
		"""
		This method generate the entry name used for looking up
		basic info in basic_info.txt file
		"""
		if self.age in range(1,5):
			return "MF/2-3"
		elif self.age in range(4,9):
			return self.sex+"/4-8"
		elif self.age in range(9,14):
			return self.sex+"/9-13"
		elif self.age in range(14,19):
			return self.sex+"/14-18"
		elif self.age in range(19,31):
			return self.sex+"/19-30"
		elif self.age in range(31,51):
			return self.sex+"/31-50"
		elif self.age >= 51:
			return self.sex+"/51+"
	
def health_monitor():
	"""
	This is the main method of health monitor
	"""
	portfolio = choose_portfolio()
	bmi = portfolio.cal_BMI()
	basic_calorie = portfolio.get_basic_calorie_info()
	intake_calorie_list = portfolio.cal_intake_list
	consume_calorie_list = portfolio.cal_consume_list
	avg_calorie_balance = cal_avg_calorie_balance(basic_calorie,intake_calorie_list,consume_calorie_list)
	print "========================================================"
	print "Health status of "+portfolio.name.replace("_"," ")
	print "The BMI is : ",bmi
	print "The average calorie balance of this week is : ",avg_calorie_balance
	print "------->"
	print "Advice:"
	generate_advice(bmi,avg_calorie_balance)
	print"=========================================================" 

def generate_advice(bmi,balance):
	"""
	This method generate a report based on the bmi and
	average calorie balance
	"""
	if bmi > 25.0:
		if balance>1000:
			print "Watch out your health!"
		elif balance<=1000 and balance >=-3500:
			print "You're a little bit overweighted!"
		else:
			print "You're on the right track!"
	else:
		if balance < 0:
			print "You have a nice body shape, Keep going!"
		elif balance>=0 and balance<=3500:
			print "It's a nice time to gain some muscle. Keep your food plan and"+\
					"exercise more!"
		else:
			print "You have a nice body shape, but eating too much can be dangerous!"+\
					"Try to cut some calorie from your food!"


def cal_avg_calorie_balance(basic,intake,consume):
	"""
	This method calculate the average balance of calorie with this formulu:
	((intake_d1-consume_d1-basic)+...+(intake_d7-consume_d7-basic))/7
	"""
	total = 0.0
	for i in range(0,7):
		total += intake[i]-consume[i]-basic
	return total/7

def choose_portfolio():
	"""
	This method list all available portolios.
	If there are not any portfolios in the current
	directory, it will require the user to enter
	information to generate a new portfolio.
	"""
	portfolio_list = glob.glob('*.portfolio')
	if len(portfolio_list) == 0:
		print "There is no available portfoilo in this directory."
		print "Please enter information to create a new portfolio:"
		return generate_new_portfolio()
	else:
		choice = raw_input("Do you want to choose an existing portfolio (please enter 1)?"+\
				"Or create a new portfolio (please enter 2)?")
		if choice == '1':
			return choose_exist_portfolio(portfolio_list)
		elif choice == '2':
			return generate_new_portfolio()
		else:
			print "Not a valid choice! Bye!"
			sys.exit(0)

def choose_exist_portfolio(portfolio_list):
	"""
	This method parses an existing portfolio
	"""
	print "Please choose a portfolio from the list (enter the number): "
	for i,p in enumerate(portfolio_list):
		print str(i+1)+" "+p
	file_num = raw_input()
	if not file_num.isdigit():
		print "Not a valid choice! Bye!"
		sys.exit(0)
		
	file_name = portfolio_list[int(file_num)-1]
	return portfolio(file_name)

def generate_new_portfolio():
	"""
	This method requires user to enter information to
	generate a new portfolio
	"""
	name = raw_input("Please enter name: ")
	name = name.replace(" ","_")
	
	file_name = name+".portfolio"
	f_port = open(file_name,'w')
	f_port.write("Name:"+name+"\n")
	
	sex = raw_input("Please choose your sex (M/F): ")
	f_port.write("Sex:"+sex+"\n")

	age = raw_input("Please enter your age: ")
	f_port.write("Age:"+age+"\n")
	
	weight = raw_input("Please enter your weight (kg): ")
	f_port.write("Weight(kg):"+weight+"kg"+"\n")

	height = raw_input("Please enter your height (cm): ")
	f_port.write("Height(cm):"+height+"cm"+"\n")

	print "Please enter Calorie Intake of each day: "
	f_port.write("Calorie Intake (k/day):"+"\n")
	cal_intake_list = []
	for d in range(1,8):
		c = raw_input("Day "+str(d)+": ")
		cal_intake_list.append(c)
		f_port.write("D"+str(d)+":"+c+"\n")

	print "Please enter Calorie Consumed by Excercise of each day: "
	f_port.write("Calorie Consumed by Excercise (k/day): "+"\n")
	cal_consume_list = []
	for d in range(1,8):
		c = raw_input("Day "+str(d)+": ")
		cal_consume_list.append(c)
		f_port.write("D"+str(d)+":"+c+"\n")

	f_port.close()

	return portfolio(name,sex,age,weight,height,cal_intake_list,cal_consume_list)
	

health_monitor()
